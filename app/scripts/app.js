'use strict';

/**
 * @ngdoc overview
 * @name roomChatClientApp
 * @description
 * # roomChatClientApp
 *
 * Main module of the application.
 */
angular
    .module('roomChatClientApp', [
        'ngAnimate',
        'ngAria',
        'ngCookies',
        'ngMessages',
        'ngResource',
        'ui.router',
        'ngSanitize',
        'ngTouch',
        'btford.socket-io',
        'uiGmapgoogle-maps',
        'toaster'
    ])
    .config(function ($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise("/login");
        $stateProvider
            .state('login', {
                url: "/login",
                templateUrl: "views/login.html",
                controller: 'LoginController'
            })
            .state('rooms', {
                url: "/rooms",
                templateUrl: 'views/rooms.html',
                controller: 'RoomController'
            })
            .state('details', {
                url: "/rooms/:roomName",
                templateUrl: 'views/main.html',
                controller: 'MainController'
            });
    });
