'use strict';

/**
 * @ngdoc function
 * @name roomChatClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the roomChatClientApp
 */
angular.module('roomChatClientApp')
    .factory('UserFactory', [
        'RccFactory',
        '$rootScope',
        function (socket, RootScope) {
            var instance = {};
            var current = null;
            instance.setUser = function(data){
                current = data;
                socket.emit('client:introduction', data);
                RootScope.$emit('user.logged');
            };
            instance.getUser = function(){
                return current;
            }

            return instance;
        }]);