'use strict';

/**
 * @ngdoc function
 * @name roomChatClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the roomChatClientApp
 */
angular.module('roomChatClientApp')
    .factory('RccFactory', ['socketFactory', function (socketFactory) {
        var myIoSocket = io.connect('http://localhost:3000');

        var mySocket = socketFactory({
            ioSocket: myIoSocket
        });

        return mySocket;
    }]);