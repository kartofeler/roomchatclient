'use strict';

/**
 * @ngdoc function
 * @name roomChatClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the roomChatClientApp
 */
angular.module('roomChatClientApp')
    .controller('MainController', [
        '$scope',
        'RccFactory',
        'UserFactory',
        '$stateParams', function ($scope, socket, User, Params) {
            socket.on('connect', function(){
                $scope.logs.push({
                    date: new Date(),
                    message: 'You connected'
                });
            });
            socket.on('server:echo', function(data){
                addLogsOnEvent(data);
            });

            socket.on('server:newMessage', function(data){
                addMessageOnEvent(data);
            });

            socket.on('server:position', function(data){
                $scope.map.center = data;
                $scope.runnerRoad.push(data);
            });

            socket.on('server:userJoinedRoom', function(data){
               addMessageOnEvent(data);
            });

            $scope.room = Params.roomName;
            $scope.newMessage = '';
            $scope.logs = [];
            $scope.messages = [];
            $scope.stroke = {
                color: '334455',
                opacity: 0.8,
                weight: 1
            };
            $scope.runnerRoad = [];

            $scope.map = {center: {latitude: 50.394880, longitude: 18.630091 }, zoom: 14 };
            $scope.options = {scrollwheel: false};

            $scope.sendMessage = function(){
                socket.emit('client:message',{
                    room: $scope.room,
                    message: $scope.newMessage,
                    user: User.getUser().username
                });
            };

            $scope.leaveRoom = function(){
                socket.emit('client:leaveRoom', {
                    room: $scope.room,
                    user: User.getUser().username
                });
            };

            function addMessageOnEvent(data){
                $scope.messages.push({
                    date: new Date(),
                    message:data.message,
                    user: data.user
                });
            }

            function addLogsOnEvent(data){
                $scope.logs.push({
                    date: new Date(),
                    message:data.message
                });
            }
        }]);
