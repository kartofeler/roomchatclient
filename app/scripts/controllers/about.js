'use strict';

/**
 * @ngdoc function
 * @name roomChatClientApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the roomChatClientApp
 */
angular.module('roomChatClientApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
