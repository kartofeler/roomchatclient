'use strict';

/**
 * @ngdoc function
 * @name roomChatClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the roomChatClientApp
 */
angular.module('roomChatClientApp')
    .controller('RoomController', [
        '$scope',
        'RccFactory',
        'UserFactory',
        'toaster',
        function ($scope, socket, User, Toast) {

            askForRoom();

            socket.on('connect', function(){
                $scope.logs.push({
                    date: new Date(),
                    message: 'You connected'
                });
            });

            socket.on('server:roomList', function(list){
                angular.forEach(list, function(value){
                   if(value.user === User.getUser().email){
                       $scope.hasRoom = true;
                   }
                });
                $scope.rooms = list;
            });

            socket.on('server:newClient', function(user){
                Toast.pop({
                    type: 'success',
                    title: 'New user: ' + user.username
                });
            });

            $scope.newMessage = '';
            $scope.logs = [];
            $scope.messages = [];
            $scope.stroke = {
                color: '334455',
                opacity: 0.8,
                weight: 1
            };
            $scope.runnerRoad = [];

            $scope.map = {center: {latitude: 50.394880, longitude: 18.630091 }, zoom: 14 };
            $scope.options = {scrollwheel: false};

            $scope.createRoom = function(roomName){
                if(!$scope.hasRoom) {
                    socket.emit('client:roomCreateReq', {
                        roomName: roomName,
                        creator: User.getUser().email
                    });
                }
            };

            $scope.joinRoom = function(room){
                socket.emit('client:joinRoom', {
                    room: room,
                    user: User.getUser().email
                })
            };


            function askForRoom(){
                socket.emit('client:roomListReq', {});
            };

            function addMessageOnEvent(data){
                $scope.messages.push({
                    date: new Date(),
                    message:data.message,
                    user: data.user
                });
            };

            function addLogsOnEvent(data){
                $scope.logs.push({
                    date: new Date(),
                    message:data.message
                });
            };
        }]);
