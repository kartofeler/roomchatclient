'use strict';

/**
 * @ngdoc function
 * @name roomChatClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the roomChatClientApp
 */
angular.module('roomChatClientApp')
    .controller('CurrentlyLoggedController', [
        '$scope',
        'UserFactory',
        '$state',
        function ($scope, User, $state) {
            $scope.user = User.getUser();
            if(User.getUser() === null){
                $state.go('login');
            }

            $scope.$on('user.logged', function(){
               $scope.user = User. getUser();
            });
        }]);
