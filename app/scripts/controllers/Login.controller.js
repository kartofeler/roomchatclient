'use strict';

/**
 * @ngdoc function
 * @name roomChatClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the roomChatClientApp
 */
angular.module('roomChatClientApp')
    .controller('LoginController', [
        '$scope',
        'UserFactory',
        '$state',
        function ($scope, User, $state) {
            $scope.userCredentials = null;
            $scope.submit = function(user){
                User.setUser(user);
                $state.go('rooms');
            }
        }]);
