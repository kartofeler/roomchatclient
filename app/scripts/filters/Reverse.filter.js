'use strict';

/**
 * @ngdoc function
 * @name roomChatClientApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the roomChatClientApp
 */
angular.module('roomChatClientApp')
    .filter('reverse', function(){
       return function(items){
           return items.slice().reverse();
       }
    });